﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Project_Test.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    //for api {postman call}
    //https://localhost:44385/api/Values/a55ff
    public class ValuesController : ControllerBase
    {
        [HttpGet("{str}")]
        public int GetVaue(string str)
        {
            string numericString = string.Empty;
            int number = 0;
            foreach (char n in str)
            {
                if ((n >= '0' && n <= '9') || n == ' ' || n == '-')
                {
                    numericString = string.Concat(numericString, n);
                }   
            }

            if (int.TryParse(numericString, out int res))
            {
                number = res;
            }

            return number;
        }
    }
}
