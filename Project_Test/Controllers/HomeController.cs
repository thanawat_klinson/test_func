﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Project_Test.Models;

namespace Project_Test.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

     
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public  IActionResult Index(Number num)
        {
            ViewBag.Number = "";
            string numericString = string.Empty;

            if (num.Numberic != null) {
                foreach (char n in num.Numberic)
                {
                    if ((n >= '0' && n <= '9') || n == ' ' || n == '-')
                    {
                        numericString = string.Concat(numericString, n);
                    }
                }

                if (int.TryParse(numericString, out int res))
                    ViewBag.Number = res;
                else
                    ViewBag.Number = "Number NotFound";
            }
   
            return View();
        }

    }
}
